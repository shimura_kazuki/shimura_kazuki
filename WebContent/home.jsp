<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<title>掲示板課題</title>
</head>
<body>
	<div class="header">
		<a href="message">新規投稿</a>
			<c:if test="${ (user.branchId == 1) && (user.departmentId == 1) }">
				<a href="manegement">ユーザー編集</a>
			</c:if>
		<a href="logout">ログアウト</a>
	</div>
	<div class="errorMessages">
		<c:if test="${ not empty errorMessages }">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" /></li>
				</c:forEach>
			</ul>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	</div>
	<div>
		<form action="./" method="get">
			<h3>日付</h3>
				<input type="date" name="start" id="start" value="${start}">
				<input type="date" name="end" id="end" value="${end}">
			<h3>カテゴリ</h3>
				<input  type="text" name="category" id="category" value="${category}">
				<input type="submit" value="絞り込み">
		</form>
	</div>
	<div>
		<c:forEach items="${messages}" var="message">
			<div class="message">
				<span class="name"><c:out value="${message.name}" /></span>
				<span class="title"><c:out value="${message.title}" /></span><br>
				<span class="text"><pre><c:out value="${message.text}" /></pre></span><br>
				<span class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></span>
			<c:if test="${ user.id == message.userId }">
				<form action="deleteMessage" method="post">
					<input type="hidden" value="${message.id}" name="id">
					<input type="submit" value="投稿削除" name="message_id" onclick="return deleteMessage()">
				</form>
			</c:if>
		</div>
		<div class="comments">
			<c:forEach items="${comments}" var="comment">
				<c:if test="${ message.id == comment.messageId }">
					<div class="comment">
						<span class="name"><c:out value="${comment.name}" /></span>
						<span class="text"><pre><c:out value="${comment.text}" /></pre></span>
						<span class="date"><fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></span>
						<c:if test="${ user.id == comment.userId }">
							<form action="deleteComment" method="post">
								<input type="hidden" value="${comment.id}" name="id">
								<input type="submit" value="コメント削除" name="comment_id" onclick="return deleteComment()">
							</form>
						</c:if>
					</div>
				</c:if>
			</c:forEach>
		</div>
		<div class="form-area">
			<form action="comment" method="post">
				<label for="text">コメント入力欄</label>
				<textarea name="text" cols="50" rows="2"></textarea><br>
				<input type="hidden" value="${message.id}" name="message_id">
				<input type="submit" value="コメント投稿">
			</form>
		</div>
	</c:forEach>
	</div>
	<div class="copyright"> Copyright(c) Shimura Kazuki </div>
</body>

    <script type="text/javascript">
function deleteMessage() {
	var result = window.confirm("投稿を削除しますか?")
       if(result){
       	return true;
	} else {
		return false;
	}
   }
</script>
<script type="text/javascript">
function deleteComment() {
	var result = window.confirm("コメントを削除しますか?")
       if(result){
       	return true;
	} else {
		return false;
	}
   }
</script>
</html>

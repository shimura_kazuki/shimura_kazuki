<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<title>Insert title here</title>
</head>
<body>
	<c:if test="${ not empty errorMessages }">
	    <div class="errorMessages">
		   <c:out value="${errorMessages}" />
	    </div>
	    <c:remove var="errorMessages" scope="session" />
	</c:if>
	<header>
  		<ul>
  			<li><a href="./">ホーム</a></li>
  			<li><a href="signup">ユーザー新規登録</a></li>
  		</ul>
	</header>
	<div>
		<c:forEach items="${users}" var="user">
			<div class="userInfo">
				<c:out value="${user.account}" />
				<c:out value="${user.name}" /><br>
				<c:out value="${user.branchName}" /><br>
				<c:out value="${user.departmentName}" /><br>
				<c:if test="${user.isStopped == 0 }">アカウント復活状態</c:if>
				<c:if test="${user.isStopped == 1 }">アカウント停止状態</c:if>

				<form action="setting" method="get">
					<input type="hidden" id="userId" name="userId" value="${user.userId}">
					<input type="submit" value=" 編集">
				 </form>

				<c:if test="${ user.userId != loginUser.id }">
					<c:if test="${ user.isStopped == 1 }">
			    		<form action="stop"  method="post">
							<input type="hidden" id="userId" name="userId" value="${user.userId}">
							<input type="hidden" id="isStopped" name="isStopped" value="0">
							<input type="submit" value="復活"  onclick = "return restart()">
						</form>
					</c:if>
					<c:if test="${ user.isStopped == 0  }">
						<form action="stop" method="post">
							 <input type="hidden" id="userId" name="userId" value="${user.userId}">
							<input type="hidden" id="isStopped" name="isStopped" value="1">
							<input type="submit" value="停止" onclick = "return stop()">
						</form>
					</c:if>
				</c:if>
			</div>
		</c:forEach>
	</div>
	<script type="text/javascript">
	function restart() {
		var result = window.confirm("復活処理をしますか?")
        if(result){
        	return true;
		} else {
			return false;
		}
    }
	</script>
	<script type="text/javascript">
		function stop() {
			var result = window.confirm("停止処理をしますか?")
	        if(result){
	        	return true;
			} else {
				return false;
			}
		}
	</script>
</body>
</html>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<title>Insert title here</title>
</head>
<body>
	<header >
		<a href="./">ホーム</a>
	</header>
	<div class="errorMessages">
		<c:if test="${ not empty errorMessages }">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	</div>
	<div class="form">
	    <form action="message" method="post">
        	<label for="title">件名</label>
			<input name="title"  value="${title}"  placeholder="件名">
			<label for="category">カテゴリ</label>
			<input name="category"  value="${category}" placeholder="カテゴリ">
			<label for="text">投稿内容</label>
            <textarea name="text" placeholder="投稿内容を記入してください">${text}</textarea>
            <input type="submit" value="投稿">
	    </form>
    </div>
</body>
</html>
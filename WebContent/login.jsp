<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    <title>ログイン</title>
</head>
<body>
	<header>
	</header>
	<div class="errorMessages">
		 <c:if test="${ not empty errorMessages }">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		<c:remove var="errorMessages" scope="session"/>
		</c:if>
	</div>
	<div class="form">
		<form action="login" method="post"><br />
			<label for="account">ログインID</label>
			<input name="account" value="${account}" id="account"/> <br />
			<label for="password">パスワード</label>
			<input name="password" type="password"  value="${password}" id="password"/> <br />
			<input type="submit" value="ログイン" /> <br />
		</form>
	</div>
	<div class="copyright"> Copyright(c) Kazuki Shimura </div>
</body>
</html>
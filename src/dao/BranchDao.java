package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {
	 public List<Branch> getBranches(Connection connection, int num) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT * from branches");

	            ps = connection.prepareStatement(sql.toString());
	            ResultSet rs = ps.executeQuery();
	            List<Branch> ret = toBranchList(rs);

	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);

	        } finally {
	            close(ps);
	        }
	    }

	    private List<Branch> toBranchList(ResultSet rs)  throws SQLException {
	        List<Branch> ret = new ArrayList<Branch>();
	        try {
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String branchName = rs.getString("name");
	                Timestamp createdDate = rs.getTimestamp("created_date");
	                Timestamp updatedDate = rs.getTimestamp("updated_date");

	                Branch branches = new Branch();

	                branches.setId(id);
	                branches.setBranchName(branchName);
	                branches.setCreatedDate(createdDate);
	                branches.setUpdatedDate(updatedDate);

	                ret.add(branches);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }
}

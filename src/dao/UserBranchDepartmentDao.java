package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;


public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> getUsers(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("users.id as userId, ");
            sql.append("users.is_stopped as isStopped, ");
            sql.append("users.branch_id as branchId, ");
            sql.append("branches.name as branchName, ");
            sql.append("users.department_id as departmentId, ");
            sql.append("departments.name as departmentName ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.id ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<UserBranchDepartment> ret = toUsers(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserBranchDepartment> toUsers(ResultSet rs)
            throws SQLException {

        List<UserBranchDepartment> ret = new ArrayList<UserBranchDepartment>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int userId = rs.getInt("userId");
                int branchId = rs.getInt("branchId");
                String branchName = rs.getString("branchName");
                int departmentId = rs.getInt("departmentId");
                String departmentName = rs.getString("departmentName");
                int isStopped = rs.getInt("isStopped");

                UserBranchDepartment user = new UserBranchDepartment();

                user.setAccount(account);
                user.setName(name);
                user.setUserId(userId);
                user.setBranchId(branchId);
                user.setBranchName(branchName);
                user.setDepartmentId(departmentId);
                user.setDepartmentName(departmentName);
                user.setIsStopped(isStopped);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}

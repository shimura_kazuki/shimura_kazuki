package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")  //URLが単数の場合はurlPatterns= {}が省略できる
public class LoginFilter implements Filter {
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		String requestUrl = ((HttpServletRequest) request).getServletPath().toString();
		String loginUrl = "/login";
		HttpSession session = ((HttpServletRequest)request).getSession();
		User user = (User) session.getAttribute("loginUser");
		String errorMessage = "ログインしてください";

		if ( requestUrl.equals(loginUrl) ) {
			chain.doFilter(request, response);
			return;
		}
		if ( user != null ) {
			chain.doFilter(request, response);
			return;
		} else {
			session.setAttribute("errorMessages", errorMessage);
			((HttpServletResponse)response).sendRedirect("login");
			return;
		}
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
	}
}

package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter( urlPatterns= {"/signup" , "/manegement" , "/setting" , "/stop"} )
public class UserAdminFilter implements Filter {
	@Override
	public void destroy() {
	}
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		User user = (User) session.getAttribute("loginUser");
		String errorMessage = "権限がないユーザーです";

		if ( ( user.getDepartmentId() == 1 ) && ( user.getBranchId() == 1 ) ) {
			chain.doFilter(request, response);
			return;
		} else {
			session.setAttribute("errorMessages", errorMessage);
			((HttpServletResponse)response).sendRedirect("./");
			return;
		}
	}
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
	}
}

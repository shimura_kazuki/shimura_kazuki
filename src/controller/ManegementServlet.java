package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBranchDepartment;
import service.UserService;

@WebServlet(urlPatterns = { "/manegement" })
public class ManegementServlet extends HttpServlet {
	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		List<UserBranchDepartment> getUsers = new UserService().getUsers();
        request.setAttribute("users", getUsers);
        HttpSession session = request.getSession();
        session.getAttribute("errorMessages");

        request.getRequestDispatcher("/manegement.jsp").forward(request, response);
    }
}

package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
	   protected void doGet(HttpServletRequest request,
	           HttpServletResponse response) throws IOException, ServletException {
	       request.getRequestDispatcher("/message.jsp").forward(request, response);
	}

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();
        User user = (User) session.getAttribute("loginUser");
        Message messages = new Message();

        messages.setTitle(request.getParameter("title"));
        messages.setCategory(request.getParameter("category"));
        messages.setText(request.getParameter("text"));
        messages.setUserId(user.getId());

        if ( isValid(request, errorMessages) ) {
            new MessageService().register(messages);
            response.sendRedirect("./");
        } else {
        	session.setAttribute("errorMessages", errorMessages);
        	String title = request.getParameter("title");
            String category = request.getParameter("category");
            String text = request.getParameter("text");
            request.setAttribute("title", title);
            request.setAttribute("category", category);
            request.setAttribute("text", text);
            request.getRequestDispatcher("message.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

        String title = request.getParameter("title");
        String category = request.getParameter("category");
        String text = request.getParameter("text");

	    if (StringUtils.isBlank(title)) {
	    	errorMessages.add("件名を入力してください");
	    } else if ( 30 <=  title.length() ) {
	    	errorMessages.add("件名は30文字以下で入力してください");
	    }

	    if (StringUtils.isBlank(category)) {
	    	errorMessages.add("カテゴリを入力してください");
	    } else if ( 10 <=  category.length() ) {
	    	errorMessages.add("カテゴリは10文字以下で入力してください");
	    }
	    if (StringUtils.isBlank(text)) {
	    	errorMessages.add("本文を入力してください");
	    } else if ( 30 <=  text.length() ) {
	    	errorMessages.add("本文は1000文字以下で入力してください");
	    }

        if (errorMessages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
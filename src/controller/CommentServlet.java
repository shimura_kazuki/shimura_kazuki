package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
//import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> comments = new ArrayList<String>();
        if ( isValid(request, comments) ) {

        	User user = (User) session.getAttribute("loginUser");
            Comment comment = new Comment();

            comment.setText(request.getParameter("text"));
            comment.setUserId(user.getId());
            comment.setMessageId(Integer.valueOf(request.getParameter("message_id")));

            new CommentService().register(comment);
            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", comments);
            response.sendRedirect("./");
        }
    }

	private boolean isValid(HttpServletRequest request, List<String> comments) {

        String text = request.getParameter("text");

        if (StringUtils.isBlank(text)) {
	    	comments.add("コメントを入力してください");
	    } else if ( 30 <=  text.length() ) {
	    	comments.add("コメントは500文字以下で入力してください");
	    }

        if (comments.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	HttpSession session = request.getSession();
        session.getAttribute("errorMessages");
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	 String account = request.getParameter("account");
         String password = request.getParameter("password");

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        UserService userService = new UserService();
        User user = userService.login(account, password);

         if ( StringUtils.isEmpty(account) ) {
	    	messages.add("アカウント名を入力してください");
	    }
        if ( StringUtils.isEmpty(password) ) {
	    	messages.add("パスワードを入力してください");
	    }
        if ( ( user != null ) && ( user.getIsStopped() != 1 ) ) {
            session.setAttribute("loginUser", user);
            response.sendRedirect("./");
        } else {
        	messages.add("アカウントまたはパスワードが間違っています");
            request.setAttribute("errorMessages", messages);
            request.setAttribute("account", account);
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
}
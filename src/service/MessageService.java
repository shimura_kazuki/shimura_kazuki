package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

    public void register(Message messages) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, messages);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<UserMessage> getMessages(String start, String end, String category) {
        Connection connection = null;
        try {
            connection = getConnection();

            String startDate = "2019-01-01 00:00:00";
            String endDate = new Timestamp(System.currentTimeMillis()).toString();
            String categoryData = null;

            if ( !StringUtils.isEmpty(start) ) {
            	 startDate = start + " 00:00:00";
            }
            if ( !StringUtils.isEmpty(end) ) {
            	endDate = end + " 23:59:59";
            }
            if ( !StringUtils.isEmpty(category) ) {
            	categoryData = "%" + category + "%" ;
            }

            UserMessageDao messageDao = new UserMessageDao();
            List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM, startDate, endDate, categoryData);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void delete(int messageId) {
		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
		    messageDao.insert(connection, messageId);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}
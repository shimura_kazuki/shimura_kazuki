package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserBranchDepartment;
import dao.UserBranchDepartmentDao;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {
	public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            if(!StringUtils.isEmpty(user.getPassword())) {
	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);
            }

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User login(String account, String password) {
        Connection connection = null;
        try {
            connection = getConnection();
            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, account, encPassword);
            commit(connection);
            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public List<UserBranchDepartment> getUsers() {
        Connection connection = null;
        try {
            connection = getConnection();
            UserBranchDepartmentDao user = new UserBranchDepartmentDao();
            List<UserBranchDepartment> ret = user.getUsers(connection);
            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public User getUser(int userId) {
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			User  ret = userDao.getUser(connection, userId);
			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User editUser) {
		Connection connection = null;
		try {
			connection = getConnection();
			if(!StringUtils.isEmpty(editUser.getPassword())) {
				String encPassword = CipherUtil.encrypt(editUser.getPassword());
				editUser.setPassword(encPassword);
			}
			UserDao userDao = new UserDao();
			userDao.update(connection, editUser);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void changeState(User userState) {
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			userDao.changeState(connection, userState);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	public boolean getExistUser(String account) {
		Connection connection = null;
		boolean existUser = false;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			existUser = userDao.getExistUser(connection, account);
			commit(connection);
			return existUser;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch (Error e) {
			rollback(connection);
		}finally {
			close(connection);
		}
		return existUser;

	}
}
